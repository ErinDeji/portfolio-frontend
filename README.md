# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Instructions

Development server with docker:

```bash
yarn docker:bash
```
Development server without docker:

```bash
yarn start
# or
npm start
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.